define([
    'uiCollection',
    'uiRegistry',
    'ko',
    'jquery',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
    'Matrixian_AddressValidator/js/validator'
], function (Collection, Registry, ko, $, $t, messageList, validator) {
    'use strict';

    return Collection.extend({
        defaults: {
            imports: {
                countryCode: '${$.parentName}.country_id:value',
                postCodeValue: '${$.parentName}.postcode:value',
                streetValue: '${$.parentName}.street.0:value',
                streetExtValue: '${$.parentName}.street.1:value',
                cityValue: '${$.parentName}.city:value',
                onChangeCountry: '${$.parentName}.country_id:value',
                onChangePostCode: '${$.parentName}.postcode:value',
                onChangeStreet: '${$.parentName}.street.0:value',
                onChangeStreetExt: '${$.parentName}.street.1:value',
                onChangeCity: '${$.parentName}.city:value',
            },
            modules: {
                country: '${$.parentName}.country_id',
                street: '${$.parentName}.street.0',
                streetExt: '${$.parentName}.street.1',
                city: '${$.parentName}.city',
                postcode: '${$.parentName}.postcode',
                regionIdInput: '${$.parentName}.region_id_input',
                suggestionSelect: '${$.name}.suggestions_select'
            },
            lookupDelay: 1000
        },
        lookupTimeout: null,
        updateAddress: ko.observable(false),
        address: ko.observable(),
        suggestions: ko.observableArray(),
        status: ko.observable(null),
        errorMsg: ko.observable(null),
        initialLoad: ko.observable(false),
        retryInitialLoad: ko.observable(true),
        isValidating: ko.observable(false),

        initialize: function() {
            this._super();

            validator.init();
            //validator.updateField('countryCode', this.countryCode());

            setTimeout(function() {
                validator.updateField('countryCode', this.countryCode);
                validator.updateField('street', this.streetValue);
                validator.updateField('streetExt', this.streetExtValue);
                validator.updateField('postalCode', this.postCodeValue);
                validator.updateField('city', this.cityValue);
                this.initialLoad(true);
                this.validateAddress(this.initialLoad());
            }.bind(this), this.lookupDelay);

            this.address.subscribe(this.updateInputAddress.bind(this));
            this.suggestions.subscribe((value) => {
                validator.setVariable('suggestionsObv', value);
            });

            return this;
        },

        validateAfterChange: function() {
            let self = this;
            this.lookupTimeout = setTimeout(function () {
                self.validateAddress();
            }, this.lookupDelay);
        },

        validateAddress: async function (initialLoad = false) {
            if (this.updateAddress() || this.isValidating() || (this.initialLoad() && !initialLoad)) {
                return;
            }
            this.isValidating(true);
            this.status(null);
            this.errorMsg(null);
            this.suggestions.removeAll();
            this.suggestionSelect().visible(false);
            this.suggestionSelect().selectedValue(null);
            let result = await validator.validateAddress();
            if (result.config.isValid()) {
                if (result.config.hasSuggestions()) {
                    this.suggestions(result.config.suggestionsObv());
                    this.status('has_suggestion');
                } else {
                    this.address(result.config.addressObv());
                }
            } else {
                if (this.initialLoad()) {
                    this.retryInitialLoad(false);
                    this.isValidating(false);
                    this.validateAddress(true);
                } else {
                    this.status(result.config.status());
                    if (result.config.errorMsg()) {
                        this.errorMsg(result.config.errorMsg());
                    }
                }
            }
            this.initialLoad(false);
            this.isValidating(false);
        },

        updateInputAddress: function (address) {
            if (typeof address === 'undefined') {
                return;
            }
            this.updateAddress(true);
            let self = this;
            let street = validator.formatStreet(address);
            let streetExt = validator.formatStreetExt(address);
            this.street().value(street.join(' ').replace(/\s+/g, ' ').trim());
            this.streetExt().value(streetExt.replace(/\s+/g, ' ').trim());
            this.postcode().value(address.postalCode);
            this.city().value(address.city);
            this.status('valid');
            setTimeout(function () {
                self.updateAddress(false);
            }, 300);
        },

        onChangeSuggestion: function(value) {
            if (typeof value === 'undefined' || !value) {
                return;
            }

            let address = ko.utils.arrayFirst(validator.getVariable('suggestionsObv')(), (item) => {
                return item.resultNumber === value;
            });

            this.address(address);
            this.suggestions.removeAll();
            this.suggestionSelect().visible(false);
            this.suggestionSelect().selectedValue(null);
        },

        onChangePostCode: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.postcode().error(false)
            }
            validator.onChange('postalCode', value);
            this.validateAfterChange();
        },

        onChangeCountry: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.postcode().error(false)
            }
            validator.onChange('countryCode', value);
            this.validateAfterChange();
        },

        onChangeStreet: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.postcode().error(false)
            }
            validator.onChange('street', value);
            this.validateAfterChange();
        },

        onChangeStreetExt: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.postcode().error(false)
            }
            validator.onChange('streetExt', value);
            this.validateAfterChange();
        },

        onChangeCity: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.postcode().error(false)
            }
            validator.onChange('city', value);
            this.validateAfterChange();
        },

        resetInputAddress: function() {
            this.street().reset();
            this.streetExt().reset();
            this.city().reset();
            this.postcode().reset();
            validator.setVariable('status', null);
        }
    });
});
