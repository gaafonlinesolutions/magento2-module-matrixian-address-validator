define([
    'Magento_Ui/js/form/components/html',
    'ko',
    'mage/translate'
], function(Html, ko, $t) {
    'use strict';

    return Html.extend({
        defaults: {
            imports: {
                addressValidatorSuggestions: '${$.parentName}:suggestions',
            },
            exports: {
                selectedValue: '${$.parentName}:onChangeSuggestion'
            },
            links: {
                visible: '${$.parentName}:suggestionsVisible'
            },
            modules: {
                suggestionSelect: '${$.name}'
            }
        },
        suggestions: ko.observableArray(),
        selectedValue: ko.observable(),
        caption: ko.observable($t('- Select an address -')),
        label: ko.observable('Suggestions'),
        selectVisible: ko.observable(false),
        singleVisible: ko.observable(false),
        singleText: ko.observable(''),

        initialize: function() {
            this._super();
            this.visible(false);

            this.selectedValue.subscribe(this.onChangeSuggestion.bind(this));

            return this;
        },

        handleClickSuggestion: function () {
            if (!this.suggestions()) {
                return;
            }
            this.selectedValue(this.suggestions().resultNumber);
        },

        addressValidatorSuggestions: function(suggestions) {
            this.selectedValue(null);
            if (suggestions.length < 1) {
                this.visible(false);
            } else {
                if (suggestions.length === 1) {
                    this.suggestions(suggestions[0]);
                    this.singleText(suggestions[0].address.join(' '));
                    this.label('Given address does not match with our records. Did you mean:');
                    this.selectVisible(false);
                    this.singleVisible(true);
                } else {
                    this.suggestions(suggestions);
                    this.label('Suggestions');
                    this.selectVisible(true);
                    this.singleVisible(false);
                }
                this.visible(true);
            }
        },

        onChangeSuggestion: function(value) {
            this.selectedValue(value);
        }
    });
});
