define([
    'Magento_Ui/js/form/components/html',
    'ko'
], function(Html, ko) {
    'use strict';

    return Html.extend({
        defaults: {
            imports: {
                checkoutAddressValidatorStatus: '${$.parentName}.matrixian_address_validator:status',
                customerAddressValidatorStatus: '${$.parentName}:status',
                checkoutAddressValidatorErrorMsg: '${$.parentName}.matrixian_address_validator:errorMsg',
                customerAddressValidatorErrorMsg: '${$.parentName}:errorMsg'
            },
            template: 'Matrixian_AddressValidator/content/address-validator-output'
        },
        valid: ko.observable(null),

        initialize: function() {
            this._super();
            this.visible(false);

            return this;
        },

        addressValidatorErrorMsg: function (message) {
            this.visible(true);
            this.valid(false);
            this.content(message);
        },

        addressValidatorStatus: function (status) {
            this.valid(null);
            if (status === 'invalid') {
                this.visible(true);
                this.valid('invalid');
                this.content('Address is invalid.');
                return;
            }

            if (status === 'valid') {
                this.visible(true);
                this.valid('valid');
                this.content('Address is valid.');
                return;
            }

            if (status === 'has_suggestion') {
                this.visible(true);
                this.valid('info');
                this.content('Select a suggestion or enter a valid address');
                return;
            }

            this.visible(false);
            this.content('');
        },

        checkoutAddressValidatorErrorMsg: function (message) {
            this.addressValidatorErrorMsg(message);
        },

        customerAddressValidatorErrorMsg: function (message) {
            this.addressValidatorErrorMsg(message);
        },

        checkoutAddressValidatorStatus: function(status) {
            this.addressValidatorStatus(status);
        },

        customerAddressValidatorStatus: function (status) {
            this.addressValidatorStatus(status);
        }
    });
});
