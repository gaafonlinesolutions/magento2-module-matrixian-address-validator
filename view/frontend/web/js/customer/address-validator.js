define([
    'uiCollection',
    'uiRegistry',
    'ko',
    'jquery',
    'Matrixian_AddressValidator/js/validator',
], function (Collection, Registry, ko, $, validator) {
    'use strict';

    return Collection.extend({
        defaults: {
            countryCodeSelector: '#country_id',
            streetSelector: '#street_1',
            street1Selector: '#street_2',
            postalCodeSelector: '#zip',
            citySelector: '#city',
            lookupDelay: 1000
        },
        lookupTimeout: null,
        countryCode: ko.observable(),
        streetValue: ko.observable(),
        street1Value: ko.observable(),
        postalCodeValue: ko.observable(),
        cityValue: ko.observable(),
        updateAddress: ko.observable(false),
        addressObv: ko.observable(),
        suggestions: ko.observableArray(),
        suggestionsVisible: ko.observable(false),
        status: ko.observable(null),
        errorMsg: ko.observable(null),
        initialLoad: ko.observable(false),
        retryInitialLoad: ko.observable(true),
        isValidating: ko.observable(false),

        initialize: function (config) {
            this._super();

            validator.init();

            this.setInitialValues(config.address);

            validator.setVariable('settings', config.settings, false);

            setTimeout(function() {
                this.initialLoad(true);
                this.validateAddress(this.initialLoad());
            }.bind(this), this.lookupDelay);

            this.countryCode.subscribe(this.onChangeCountryCode.bind(this));
            this.streetValue.subscribe(this.onChangeStreet.bind(this));
            this.street1Value.subscribe(this.onChangeStreetExt.bind(this));
            this.postalCodeValue.subscribe(this.onChangePostalCode.bind(this));
            this.cityValue.subscribe(this.onChangeCity.bind(this));

            this.addressObv.subscribe(this.updateInputAddress.bind(this));
            this.suggestions.subscribe((value) => {
                validator.setVariable('suggestionsObv', value);
            });

            return this;
        },

        setInitialValues: function (address) {
            this.countryCode(address.country_id);
            this.streetValue(address.street);
            this.street1Value(address.streetExt);
            this.postalCodeValue(address.postalCode);
            this.cityValue(address.city);
            validator.updateField('countryCode', address.country_id);
            validator.updateField('street', address.street);
            validator.updateField('streetExt', address.streetExt);
            validator.updateField('postalCode', address.postalCode);
            validator.updateField('city', address.city);
        },

        onChangeSuggestion: function (value) {
            let self = this;
            if (typeof value === 'undefined' || !value) {
                return;
            }

            let address = ko.utils.arrayFirst(validator.getVariable('suggestionsObv')(), (item) => {
                return item.resultNumber === value;
            });

            this.addressObv(address);
            this.suggestions.removeAll();
            this.suggestionsVisible(false);
        },

        onChangeCountryCode: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.removeError(this.countryCodeSelector);
            }
            validator.onChange('countryCode', value);
            this.validateAfterChange();
        },

        onChangeStreet: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.removeError(this.streetSelector);
            }
            validator.onChange('street', value);
            this.validateAfterChange();
        },

        onChangeStreetExt: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.removeError(this.street1Selector);
            }
            validator.onChange('streetExt', value);
            this.validateAfterChange();
        },

        onChangePostalCode: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.removeError(this.postalCodeSelector);
            }
            validator.onChange('postalCode', value);
            this.validateAfterChange();
        },

        onChangeCity: function(value) {
            if (value === '') {
                validator.setVariable('status', 'empty');
                return this.removeError(this.citySelector);
            }
            validator.onChange('city', value);
            this.validateAfterChange();
        },

        validateAfterChange: function() {
            let self = this;
            this.lookupTimeout = setTimeout(function () {
                self.validateAddress();
            }, this.lookupDelay);
        },

        validateAddress: async function (initialLoad = false) {
            if (this.updateAddress() || this.isValidating() || (this.initialLoad() && !initialLoad)) {
                return;
            }
            this.isValidating(true);
            this.status(null);
            this.errorMsg(null);
            let result = await validator.validateAddress();
            if (result.config.isValid()) {
                if (result.config.hasSuggestions()) {
                    this.suggestions(result.config.suggestionsObv());
                    this.status('has_suggestion');
                } else {
                    this.addressObv(result.config.addressObv());
                }
            } else {
                if (this.initialLoad()) {
                    this.retryInitialLoad(false);
                    this.isValidating(false);
                    this.validateAddress(true);
                } else {
                    this.status(result.config.status());
                    if (result.config.errorMsg()) {
                        this.errorMsg(result.config.errorMsg());
                    }
                }
            }
            this.initialLoad(false);
            this.isValidating(false);
        },

        updateInputAddress: function (address) {
            if (typeof address === 'undefined') {
                return;
            }
            this.updateAddress(true);
            let self = this;
            let street = validator.formatStreet(address);
            let streetExt = validator.formatStreetExt(address);
            this.streetValue(street.join(' ').replace(/\s+/g, ' ').trim());
            this.street1Value(streetExt.replace(/\s+/g, ' ').trim());
            this.postalCodeValue(address.postalCode);
            this.cityValue(address.city);
            this.status('valid');
            setTimeout(function () {
                self.updateAddress(false);
            }, this.lookupDelay);
        },

        showError: function (selector, msg) {
            this.removeError(selector);
            let html = '<div class="mage-error" generated="true">'+ msg +'</div>';
            $(selector).append(html);
        },

        removeError: function (selector) {
            $(selector).find('.mage-error').not('input').remove();
        }
    })
});
