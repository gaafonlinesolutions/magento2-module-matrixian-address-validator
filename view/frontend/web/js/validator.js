define([
    'ko',
    'uiComponent',
    'jquery',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
], function (ko, Component, $, $t, messageList) {
    "use strict";

    let viewModel = null;

    class validatorViewModel {
        config = {
            settings: (window.checkoutConfig) ? window.checkoutConfig.addressValidator : [],
            endpoint: 'address-validator/V1/address/validate',
            nrBeforeStreet: ['FR', 'GB', 'LU', 'US'],
            streetExtFields: ['apartment', 'block', 'door', 'flat', 'floor', 'stair'],
            lookupTimeout: null,
            addressObv: ko.observable(),
            suggestionsObv: ko.observableArray(),
            hasSuggestions: ko.observable(false),
            isValid: ko.observable(null),
            status: ko.observable(null),
            errorMsg: ko.observable(false),
            updateAddressObv: ko.observable(false),
            loading: ko.observable(false),
            suggestionSelectVisible: ko.observable(false),
            fields: {
                countryCode: "",
                street: "",
                streetExt: "",
                postalCode: "",
                city: ""
            }
        }
    }

    return {

        init: function () {
            viewModel = new validatorViewModel();
        },

        onChange: function(field, value) {
            clearTimeout(viewModel.config.lookupTimeout);
            viewModel.config.fields[field] = value;
        },

        validateAddress: function () {
            return new Promise((resolve, reject) => {
                let self = this;
                $('body').trigger('processStart');
                if (self.requiredFieldsFilled()) {
                    let postData = {
                        countryCode: viewModel.config.fields.countryCode,
                        street: viewModel.config.fields.street,
                        streetExt: viewModel.config.fields.streetExt ?? '****',
                        postalCode: viewModel.config.fields.postalCode,
                        city: viewModel.config.fields.city ?? '****'
                    }

                    $.ajax({
                        url: viewModel.config.settings.baseUrl + viewModel.config.endpoint,
                        type: 'post',
                        data: JSON.stringify(postData),
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        success: function (data) {
                            if (data.error) {
                                //messageList.addErrorMessage({message: $t("An error occurred during address validation")});
                                $('body').trigger('processStop');
                                viewModel.config.status('error');
                                viewModel.config.errorMsg(data.error);
                                resolve(viewModel);
                            }

                            let address = JSON.parse(data.address);
                            let suggestions = JSON.parse(data.suggestions);
                            viewModel.config.errorMsg(false);

                            if (data.valid) {
                                viewModel.config.isValid(true);
                                if (suggestions && suggestions.length > 0) {
                                    viewModel.config.hasSuggestions(true);
                                    viewModel.config.suggestionsObv.removeAll();
                                    ko.utils.arrayPushAll(viewModel.config.suggestionsObv, suggestions);
                                } else {
                                    let isAddressEqual = self.compareAddress(postData, address);
                                    if (isAddressEqual) {
                                        viewModel.config.hasSuggestions(false);
                                        viewModel.config.addressObv(address);
                                    } else {
                                        viewModel.config.hasSuggestions(true);
                                        viewModel.config.suggestionsObv.removeAll();
                                        viewModel.config.suggestionsObv.push(address);
                                    }
                                }
                            } else {
                                viewModel.config.isValid(false);
                                viewModel.config.status('invalid');
                            }
                            $('body').trigger('processStop');
                            resolve(viewModel);
                        },
                        error: function (jqXHR) {
                            console.log(jqXHR);
                            messageList.addErrorMessage({message: $t("An error occurred during address validation")});
                            $('body').trigger('processStop');
                            viewModel.config.status('error');
                            resolve(viewModel);
                        }
                    });
                } else {
                    $('body').trigger('processStop');
                    resolve(viewModel);
                }
            });
        },

        compareAddress: function (inputAddress, address) {
            let isEqual = true;
            let street = this.formatStreet(address);
            let streetExt = this.formatStreetExt(address);
            let TypeOf = ['function', 'object', 'undefined'];

            if (inputAddress.street && inputAddress.street.replace(/\s+/g, ' ').trim() !== street.join(' ').replace(/\s+/g, ' ').trim() && !TypeOf.includes(typeof inputAddress.street)) {
                isEqual = false;
            }

            if (inputAddress.streetExt && inputAddress.streetExt !== streetExt && !TypeOf.includes(typeof inputAddress.streetExt)) {
                isEqual = false;
            }

            if (inputAddress.postalCode && inputAddress.postalCode !== address.postalCode && !TypeOf.includes(typeof inputAddress.postalCode)) {
                isEqual = false;
            }

            if (inputAddress.city && inputAddress.city !== '****' && inputAddress.city !== address.city && !TypeOf.includes(typeof inputAddress.city)) {
                isEqual = false;
            }

            return isEqual;
        },

        formatStreet: function (address) {
            if (typeof address === 'undefined') {
                return;
            }
            if (viewModel.config.nrBeforeStreet.includes(address.countryIso2)) {
                return [address.houseNumber, (address.houseNumberExt) ? address.houseNumberExt : '', address.street];
            } else {
                return [address.street, address.houseNumber, (address.houseNumberExt) ? address.houseNumberExt : ''];
            }
        },

        formatStreetExt: function (address) {
            if (typeof address === 'undefined') {
                return;
            }
            let streetExt = '';
            $.each(viewModel.config.streetExtFields, (i, field) => {
                if (address[field] && address[field] !== '' && address[field].length > 0) {
                    streetExt += (address[field] ?? '') + ' ';
                }
            });
            streetExt = streetExt.trimEnd();
            return streetExt;
        },

        requiredFieldsFilled: function () {
            let valid = true;

            if (viewModel.config.fields.postalCode === '' || typeof (viewModel.config.fields.postalCode) === 'undefined') {
                valid = false;
            }

            if (viewModel.config.fields.street === '' || typeof (viewModel.config.fields.street) === 'undefined') {
                valid = false;
            }

            return valid;
        },

        setVariable(variable, value, isObservable = true) {
            if (!isObservable) {
                viewModel.config[variable] = value;
            } else {
                viewModel.config[variable](value);
            }
        },

        getVariable: function (variable) {
            return viewModel.config[variable];
        },

        updateField: function (field, value) {
            viewModel.config.fields[field] = value;
        }
    }
});
