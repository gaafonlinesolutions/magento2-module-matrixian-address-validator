<?php
namespace Matrixian\AddressValidator\Api;

use Matrixian\AddressValidator\Api\Data\ApiResultInterface;

interface ApiClientInterface
{
    /**
     * @return ApiResultInterface
     */
    public function validate(): ApiResultInterface;

    /**
     * @param string $username
     * @param string $password
     * @return string
     */
    public function authorize(string $username, string $password): string;
}
