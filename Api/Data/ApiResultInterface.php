<?php
namespace Matrixian\AddressValidator\Api\Data;

interface ApiResultInterface
{
    const VALID = 'valid';
    const ADDRESS = 'address';
    const SUGGESTIONS = 'suggestions';
    const ERROR = 'error';

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return mixed
     */
    public function getAddress();

    /**
     * @param string $addressData
     * @return mixed
     */
    public function setAddress(string $addressData);

    /**
     * @return mixed
     */
    public function getSuggestions();

    /**
     * @param string $suggestions
     * @return mixed
     */
    public function setSuggestions(string $suggestions);

    /**
     * @return mixed
     */
    public function getError();

    /**
     * @param string $error
     * @return mixed
     */
    public function setError(string $error);
}
