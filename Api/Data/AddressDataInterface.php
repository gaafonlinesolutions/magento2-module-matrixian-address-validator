<?php
namespace Matrixian\AddressValidator\Api\Data;

interface AddressDataInterface
{
    const COUNTRY_CODE = 'countryCode';
    const POSTAL_CODE = 'postalCode';
    const HOUSE_NUMBER = 'houseNumber';
    const HOUSE_NUMBER_EXT = 'houseNumberExt';
    const STREET = 'street';
    const CITY = 'city';

    /**
     * @return mixed
     */
    public function getCountryCode();

    /**
     * @param string $countryCode
     * @return mixed
     */
    public function setCountryCode(string $countryCode);

    /**
     * @return mixed
     */
    public function getPostalCode();

    /**
     * @param string $postalCode
     * @return mixed
     */
    public function setPostalCode(string $postalCode);

    /**
     * @return mixed
     */
    public function getHouseNumber();

    /**
     * @param string $houseNumber
     * @return mixed
     */
    public function setHouseNumber(string $houseNumber);

    /**
     * @return mixed
     */
    public function getHouseNumberExt();

    /**
     * @param string $houseNumberExt
     * @return mixed
     */
    public function setHouseNumberExt(string $houseNumberExt);

    /**
     * @return mixed
     */
    public function getStreet();

    /**
     * @param string $street
     * @return mixed
     */
    public function setStreet(string $street);

    /**
     * @return mixed
     */
    public function getCity();

    /**
     * @param string $city
     * @return mixed
     */
    public function setCity(string $city);
}
