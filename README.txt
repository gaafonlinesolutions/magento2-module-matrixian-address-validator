=== Matrixian Address Validator ===
Contributors: gaafdigitalagency, matrixian
Donate link: https://www.matrixiangroup.com/en/
Tags: address, address validation, billing, validation, magento, magento 2
Requires PHP: 7.4
Tested up to: 2.4.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Address Validator for Magento 2.

== Description ==

Address Validator for Magento 2

The Magento 2 Matrixian Address Validator module uses the [Matrixian Address Validator API](https://platform.matrixiangroup.com/api/documentation "Matrixian API documentation") to validate and correct billing/shipping addresses for the following countries:

* Austria
* Belgium
* Czech Republic
* Denmark
* Germany
* France
* Italy
* Luxembourg
* Spain
* Sweden
* The Netherlands
* United Kingdom
* United States

Stop wasting time and money with undelivered packages or having to reach out to customers to get the correct addresses.

SUPPORTED PLUGINS / COMPATIBLE FORMS

The plugin integrates seamlessly with:

* Magento 2

KEY FEATURES

Automatic validation of billing and shipping addresses
Customers get a real-time suggestion if they enter an incomplete address
Easy-to-use, automatically integrates with the checkout page and the customer address book

GETTING STARTED

Using the module requires a Matrixian Group Platform account. You can register at [Matrixian Platform](https://platform.matrixiangroup.com/register "Matrixian Platform registration")


== Installation ==

1. Install the composer package with 'composer require gaaf/matrixian-address-validator'
2. Execute the 'bin/magento setup:upgrade' command to install the module
3. In your admin environment go to Configuration > Sales > Matrixian Address Validator
4  Set 'Enabled' to 'Yes'
5. Fill out your Matrixian account credentials
6. Address Validator is now active


== Frequently Asked Questions ==

= Does it include auto-complete functionality? =

No, this plugin is used for address validation purposes only.

= Does it work for my country? =

We do not support all countries at the moment, but we will add support for new countries in the future. You can find a list of supported countries in the plugin description.

= Is this plugin free to use? =

No, this plugin requires an Matrixian Platform account. You will be charged based on the amount of validations you run.

= Where can I get a Matrixian account? =

You can register at [Matrixian Platform](https://platform.matrixiangroup.com/register "Matrixian Platform registration")

== Changelog ==

= 1.0 =
* Support validation for all listed countries.
* Support address validation for billing and shipping addresses in both the checkout and the customer address book.
* Initial version.

== Upgrade Notice ==

= 1.0 =
Add address validation to default billing and shipping fields for all listed countries.
