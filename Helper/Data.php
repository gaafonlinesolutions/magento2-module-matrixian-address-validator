<?php
namespace Matrixian\AddressValidator\Helper;

use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    protected StoreManagerInterface $storeManager;
    protected TypeListInterface $typeList;
    protected Pool $cacheFrontendPool;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        TypeListInterface $typeList,
        Pool $cacheFrontendPool
    ) {
        parent::__construct($context);

        $this->storeManager = $storeManager;
        $this->typeList = $typeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isEnabled(): bool
    {
        return $this->scopeConfig->getValue('address_validator/general/enabled', ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isSuggestionsDisabled(): bool
    {
        return !$this->scopeConfig->getValue('address_validator/general/suggestions_enabled', ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSettings(): array
    {
        return [
            'enabled' => $this->isEnabled(),
            'suggestionsDisabled' => $this->isSuggestionsDisabled(),
            'baseUrl' => $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB)
        ];

    }

    /**
     * @param string $type
     */
    public function flushCacheByType(string $type): void
    {
        $this->typeList->cleanType($type);
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
