<?php
namespace Matrixian\AddressValidator\Helper;

use Matrixian\AddressValidator\Api\ApiClientInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Api extends AbstractHelper
{
    protected Json $serializer;
    protected LoggerInterface $logger;
    protected StoreManagerInterface $storeManager;
    protected ApiClientInterface $apiClient;

    public function __construct(
        Context $context,
        Json $serializer,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $path
     * @param string|null $scope
     * @param int|null $scopeId
     * @return mixed
     */
    public function getConfigValue($path, string $scope = null, int $scopeId = null)
    {
        return $this->scopeConfig->getValue($path, $scope, $scopeId);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getApiUrl(): string
    {
        return $this->scopeConfig->getValue('address_validator/general/api_url', ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
    }

    /**
     * @param string $url
     * @param string $method
     * @param bool $isTokenRequest
     * @param array $data
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function sendRequest(string $url, string $method = 'GET', bool $isTokenRequest = false, array $data = []): array
    {
        $ch = curl_init($url);
        $headers = ['Content-Type: application/json'];
        if (!$isTokenRequest) {
            $headers[] = 'Authorization: Bearer ' . $this->getToken();
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $result = [];
        $result['code'] = $responseCode;
        if ($responseCode !== 200 || curl_errno($ch)) {
            $errorMsg = curl_error($ch);
            if (!empty($errorMsg)) {
                throw new \Exception("[AddressValidator][$responseCode] $errorMsg");
            }
            if (($isTokenRequest && $responseCode !== 200) || $responseCode !== 404) {
                throw new \Exception("[AddressValidator][$responseCode] $response");
            }
        }

        $result['data'] = json_decode($response, true);
        return $result;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isTokenValid(): bool
    {
        if(!empty($this->scopeConfig->getValue('address_validator/token/data'))) {
            $tokenData = $this->serializer->unserialize($this->scopeConfig->getValue('address_validator/token/data'));
            if (time() > strtotime($tokenData['data']['.expires'])) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getToken(): string
    {
        if (!empty($this->scopeConfig->getValue('address_validator/token/data')))
        $tokenData = $this->serializer->unserialize($this->scopeConfig->getValue('address_validator/token/data'));
        return $tokenData['data']['access_token'];
    }
}
