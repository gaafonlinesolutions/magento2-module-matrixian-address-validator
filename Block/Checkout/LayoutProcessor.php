<?php
namespace Matrixian\AddressValidator\Block\Checkout;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class LayoutProcessor extends AbstractBlock implements LayoutProcessorInterface
{
    private ScopeConfigInterface $scopeConfig;

    private StoreManagerInterface $storeManager;

    private Manager $moduleManager;

    private bool $forceFieldsOrder = false;

    /**
     * __construct function.
     *
     * @access public
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param array $data (default: [])
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Manager $moduleManager,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->storeManager = $storeManager;
        $this->moduleManager = $moduleManager;

        parent::__construct($context, $data);
    }


    /**
     * process function.
     *
     * @access public
     * @param mixed $jsLayout
     * @return array
     */
    public function process($jsLayout): array
    {
        $this->forceFieldsOrder = (bool)$this->scopeConfig->getValue('address_validator/advanced/force_fields_order', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getCode());
        if ($this->moduleManager->isEnabled('Onestepcheckout_Iosc') && !$this->forceFieldsOrder) {
            return $jsLayout;
        }

        $enabled = $this->scopeConfig->getValue('address_validator/general/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getCode());

        if ($enabled && isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset'])) {
            $jsLayout = $this->processShippingFields($jsLayout);
            $jsLayout = $this->processBillingFields($jsLayout);
        }

        return $jsLayout;
    }


    /**
     * processShippingFields function.
     *
     * @access public
     * @param mixed $result
     * @return array
     */
    public function processShippingFields($result): array
    {
        $shippingFields = $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];

        $shippingFields = $this->changeAddressFieldPosition($shippingFields);

        $result['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']
        ['shipping-address-fieldset']['children'] = $shippingFields;

        return $result;
    }


    /**
     * processBillingFields function.
     *
     * @access public
     * @param mixed $result
     * @return array
     */
    public function processBillingFields($result): array
    {
        if (isset($result['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list'])) {

            $paymentForms = $result['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['payments-list']['children'];

            foreach ($paymentForms as $paymentMethodForm => $paymentMethodValue) {
                $paymentMethodCode = str_replace('-form', '', $paymentMethodForm);

                if (!isset($result['components']['checkout']['children']['steps']['children']['billing-step']
                    ['children']['payment']['children']['payments-list']['children'][$paymentMethodCode . '-form'])) {
                    continue;
                }

                $billingFields = $result['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']
                ['payments-list']['children'][$paymentMethodCode . '-form']['children']['form-fields']['children'];

                $shippingFields = $result['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];

                $billingFields = array_merge($billingFields, array_intersect_key($shippingFields, ['matrixian_address_validator' => 1]));
                $billingFields = $this->changeAddressFieldPosition($billingFields);

                $result['components']['checkout']['children']['steps']['children']['billing-step']
                ['children']['payment']['children']['payments-list']['children'][$paymentMethodCode . '-form']
                ['children']['form-fields']['children'] = $billingFields;
            }
        }

        return $result;
    }


    /**
     * changeAddressFieldPosition function.
     *
     * @access public
     * @param mixed $addressFields
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function changeAddressFieldPosition($addressFields): array
    {
        if (($this->moduleManager->isEnabled('Amasty_Checkout') || $this->moduleManager->isEnabled('Mageplaza_Osc')) && !$this->forceFieldsOrder) {
            if (isset($addressFields['postcode']) && isset ($addressFields['city'])) {
                if ($addressFields['postcode']['sortOrder'] > $addressFields['city']['sortOrder']) {
                    $addressFields['matrixian_address_validator']['sortOrder'] = ($addressFields['postcode']['sortOrder'] + 1);
                } else {
                    $addressFields['matrixian_address_validator']['sortOrder'] = ($addressFields['city']['sortOrder'] + 1);
                }
            }

            $this->reorderFields($addressFields, $addressFields['matrixian_address_validator']['sortOrder']);

            $test = array_search($addressFields['matrixian_address_validator']['sortOrder'], array_column($addressFields, 'sortOrder'));
            return $addressFields;
        }

        if (isset($addressFields['country_id'])) {
            $addressFields['country_id']['sortOrder'] = '900';
        }

        if (isset($addressFields['street'])) {
            $addressFields['street']['sortOrder'] = '910';
        }

        if (isset($addressFields['postcode'])) {
            $addressFields['postcode']['sortOrder'] = '920';
        }

        if (isset($addressFields['city'])) {
            $addressFields['city']['sortOrder'] = '930';
        }

        if (isset($addressFields['matrixian_address_validator'])) {
            $addressFields['matrixian_address_validator']['sortOrder'] = '930';
        }

        if (isset($addressFields['region'])) {
            $addressFields['region']['sortOrder'] = '940';
        }

        if (isset($addressFields['region_id'])) {
            $addressFields['region_id']['sortOrder'] = '945';
        }

        return $addressFields;
    }

    private function reorderFields(array &$addressFields, int $sortOrder)
    {
        foreach ($addressFields as $key => $addressField) {
            if ($key !== 'matrixian_address_validator' && array_key_exists('sortOrder', $addressField) && $addressField['sortOrder'] === $sortOrder) {
                $sortOrder++;
                $addressFields[$key]['sortOrder'] = $sortOrder;
            }
        }
    }
}
