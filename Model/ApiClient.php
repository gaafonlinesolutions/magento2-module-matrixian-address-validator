<?php
namespace Matrixian\AddressValidator\Model;

use Matrixian\AddressValidator\Api\ApiClientInterface;
use Matrixian\AddressValidator\Api\Data\AddressDataInterface;
use Matrixian\AddressValidator\Api\Data\ApiResultInterface;
use Matrixian\AddressValidator\Helper\AddressSplitter\AddressSplitter;
use Matrixian\AddressValidator\Helper\Api;
use Matrixian\AddressValidator\Helper\Data;
use Matrixian\AddressValidator\Model\Data\ApiResult;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class ApiClient implements ApiClientInterface
{
    const GRANT_TYPE = 'password';
    const CHECK_ENDPOINT = '/address/check';
    const TOKEN_ENDPOINT = '/token';

    protected Api $apiHelper;
    protected Request $request;
    protected AddressSplitter $addressSplitter;
    protected WriterInterface $configWriter;
    protected Json $serializer;
    protected StoreManagerInterface $storeManager;
    protected LoggerInterface $logger;
    protected Data $customHelper;

    public function __construct(
        Api $apiHelper,
        Request $request,
        AddressSplitter $addressSplitter,
        WriterInterface $configWriter,
        Json $serializer,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        Data $customHelper
    ) {
        $this->apiHelper = $apiHelper;
        $this->request = $request;
        $this->addressSplitter = $addressSplitter;
        $this->configWriter = $configWriter;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->customHelper = $customHelper;
    }

    /**
     * Matrixian Endpoint `/address/check`
     * @return ApiResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    public function validate(): ApiResultInterface
    {
        try {
            if (!$this->apiHelper->isTokenValid()) {
                $username = $this->apiHelper->getConfigValue('address_validator/auth/username', ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
                $password = $this->apiHelper->getConfigValue('address_validator/auth/password', ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
                $this->authorize($username, $password);
            }
            $data = $this->request->getBodyParams();
            $address = $this->addressSplitter->splitAddress($data['street'] . ' ' . $data['streetExt']);
            $data['street'] = $address['streetName'];
            $data['houseNumber'] = $address['houseNumber'];
            $data['houseNumberExt'] = $address['additionToAddress2'] ?? '****';
            if (empty($data['city'])) {
                //$data['city'] = '****';
            }
            unset($data['streetExt']);
            $apiUrl = $this->apiHelper->getApiUrl() . self::CHECK_ENDPOINT . '?' . http_build_query($data);
            $this->logger->debug('TEST API ' . $apiUrl);
            $response = $this->apiHelper->sendRequest($apiUrl, 'GET', false, []);
            $result = new ApiResult();
            if ($response['code'] === 404) {
                $result->setIsValid(false);
                return $result;
            }
            $result->setIsValid(true);
            $result->setAddress(json_encode($response['data'][0]));
            if (count($response['data']) > 1) {
                $result->setSuggestions(json_encode($response['data']));
            }
            return $result;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            $result = new ApiResult();
            $result->setIsValid(false);
            $result->setError($e->getCode() . ': ' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $result;
        }
    }

    /**
     * Matrixian Endpoint `/token`
     * @param string $username
     * @param string $password
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function authorize(string $username, string $password): string
    {
        try {
            $tokenData = $this->apiHelper->sendRequest($this->apiHelper->getApiUrl() . self::TOKEN_ENDPOINT, 'POST', true, [
                'grant_type' => self::GRANT_TYPE,
                'username' => $username,
                'password' => $password,
            ]);
            $this->configWriter->save('address_validator/token/data', $this->serializer->serialize($tokenData));
            $this->customHelper->flushCacheByType('config');
            return $tokenData['data']['access_token'];
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return '';
        }
    }
}
