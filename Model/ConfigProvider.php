<?php

namespace Matrixian\AddressValidator\Model;

use Matrixian\AddressValidator\Helper\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Customer\Model\Session;

/**
 * Class CustomConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    protected Data $helper;

    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * getConfig function.
     *
     * @access public
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        return [
            'addressValidator' => $this->helper->getSettings(),
        ];
    }
}
