<?php
namespace Matrixian\AddressValidator\Model\Data;

use Matrixian\AddressValidator\Api\Data\AddressDataInterface;
use Magento\Framework\DataObject;

class AddressData extends DataObject implements AddressDataInterface
{
    /**
     * @return mixed|null
     */
    public function getCountryCode()
    {
        return $this->_getData(self::COUNTRY_CODE);
    }

    /**
     * @param string $countryCode
     * @return void
     */
    public function setCountryCode(string $countryCode)
    {
        $this->setData(self::COUNTRY_CODE, $countryCode);
    }

    /**
     * @return mixed|null
     */
    public function getPostalCode()
    {
        return $this->_getData(self::POSTAL_CODE);
    }

    /**
     * @param string $postalCode
     * @return void
     */
    public function setPostalCode(string $postalCode)
    {
        $this->setData(self::POSTAL_CODE, $postalCode);
    }

    /**
     * @return mixed|null
     */
    public function getHouseNumber()
    {
        return $this->_getData(self::HOUSE_NUMBER);
    }

    /**
     * @param string $houseNumber
     * @return void
     */
    public function setHouseNumber(string $houseNumber)
    {
        $this->setData(self::HOUSE_NUMBER, $houseNumber);
    }

    /**
     * @return mixed|null
     */
    public function getHouseNumberExt()
    {
        return $this->_getData(self::HOUSE_NUMBER_EXT);
    }

    /**
     * @param string $houseNumberExt
     * @return void
     */
    public function setHouseNumberExt(string $houseNumberExt)
    {
        $this->setData(self::HOUSE_NUMBER_EXT, $houseNumberExt);
    }

    /**
     * @return mixed|null
     */
    public function getStreet()
    {
        return $this->_getData(self::STREET);
    }

    /**
     * @param string $street
     * @return void
     */
    public function setStreet(string $street)
    {
        $this->setData(self::STREET, $street);
    }

    /**
     * @return mixed|void
     */
    public function getCity()
    {
        return $this->_getData(self::CITY);
    }

    /**
     * @param string $city
     * @return void
     */
    public function setCity(string $city)
    {
        $this->setData(self::CITY, $city);
    }
}
