<?php
namespace Matrixian\AddressValidator\Model\Data;

use Matrixian\AddressValidator\Api\Data\AddressDataInterface;
use Matrixian\AddressValidator\Api\Data\ApiResultInterface;
use Magento\Framework\DataObject;

class ApiResult extends DataObject implements ApiResultInterface
{
    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->_getData(self::VALID);
    }

    /**
     * @param bool $valid
     */
    public function setIsValid(bool $valid): void
    {
        $this->setData(self::VALID, $valid);
    }

    /**
     * @return mixed|null
     */
    public function getAddress()
    {
        return $this->_getData(self::ADDRESS);
    }

    /**
     * @param string $addressData
     * @return void
     */
    public function setAddress(string $addressData): void
    {
        $this->setData(self::ADDRESS, $addressData);
    }

    /**
     * @return mixed|null
     */
    public function getSuggestions()
    {
        return $this->_getData(self::SUGGESTIONS);
    }

    /**
     * @param string $suggestions
     * @return void
     */
    public function setSuggestions(string $suggestions): void
    {
        $this->setData(self::SUGGESTIONS, $suggestions);
    }

    /**
     * @return mixed|null
     */
    public function getError()
    {
        return $this->_getData(self::ERROR);
    }

    public function setError(string $error)
    {
        $this->setData(self::ERROR, $error);
    }
}
